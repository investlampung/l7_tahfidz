@extends('layouts.user.beranda')

@section('content')
<!-- Kampung Tahfidz FIX -->
<section class="banner-area relative" id="home">
  <div class="overlay overlay-bg"></div>
  <div class="container">
    <div class="row fullscreen d-flex justify-content-center align-items-center">
      <div class="banner-content col-lg-9 col-md-12 justify-content-center">
        <h1>
          Kampung Tahfidz Gadingrejo
        </h1>
        <p class="text-white mx-auto">
          @foreach($beranda as $ber)
          @if($ber->beranda_key==='Text Slide')
          {!!$ber->beranda_value!!}
          @endif
          @endforeach
        </p>
        <a href="#hubungi" class="primary-btn header-btn text-uppercase mt-10">Hubungi Kami</a>
      </div>
    </div>
  </div>
</section>
<!-- Kampung Tahfidz FIX -->
<section class="open-hour-area">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-12 open-hour-wrap">
        <h1>Kampung Tahfidz Gadingrejo</h1>
        <p>
          @foreach($beranda as $ber)
          @if($ber->beranda_key==='Kampung Tahfidz')
          {!!$ber->beranda_value!!}
          @endif
          @endforeach
        </p>
      </div>
    </div>
  </div>
</section>
<!-- Rencana FIX -->
<section class="service-area section-gap">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-md-12 pb-40 header-text text-center">
        <h1 class="pb-10">Rencana Lokasi Kampung Tahfidz Gadingrejo</h1>
        <p>
          @foreach($beranda as $ber)
          @if($ber->beranda_key==='Rencana')
          {!!$ber->beranda_value!!}
          @endif
          @endforeach
        </p>
      </div>
    </div>
    <div class="row">
      @foreach($gambar as $image)
      @if($image->gambar_key==='Gambar Rencana 1')
      <div class="col-md-4">
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" class="img-fluid" alt="Responsive image"><br><br>
      </div>
      @endif
      @endforeach
      @foreach($gambar as $image)
      @if($image->gambar_key==='Gambar Rencana 2')
      <div class="col-md-4">
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" class="img-fluid" alt="Responsive image"><br><br>
      </div>
      @endif
      @endforeach
      @foreach($gambar as $image)
      @if($image->gambar_key==='Gambar Rencana 3')
      <div class="col-md-4">
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" class="img-fluid" alt="Responsive image"><br><br>
      </div>
      @endif
      @endforeach
    </div>
    <div class="row">
      <div class="col-lg-12" align="center">
        <a href="#hubungi" class="primary-btn header-btn text-uppercase mt-10">Hubungi Kami</a>
      </div>
    </div>
  </div>
</section>
<!-- FIX -->
<section class="home-about-area section-gap relative">
  <div class="container">
    <div class="row align-items-center justify-content-end">
      <div class="col-lg-6" align="center">
        @foreach($gambar as $image)
        @if($image->gambar_key==='Gambar Fasilitas')
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" class="image-fasilitas" alt="">
        @endif
        @endforeach
      </div>
      <div class="col-lg-6 no-padding home-about-right">
        <h1 class="text-white">
          Fasilitas Kampung <br>
          Tahfidz Gadingrejo
        </h1>
        <p>
          @foreach($beranda as $ber)
          @if($ber->beranda_key==='Fasilitas')
          {!!$ber->beranda_value!!}
          @endif
          @endforeach
          <br><br>
          <a href="#hubungi" class="primary-btn header-btn text-uppercase mt-10">Hubungi Kami</a>
        </p>
      </div>
    </div>
  </div>
</section>
<!-- FIX -->
<section class="testomial-area section-gap">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="menu-content pb-60 col-lg-8">
        <div class="title text-center">
          <h1 class="mb-10">Mengapa Anda Harus Memilikinya?</h1>
          <p>Berikut Kelebihan dari Kampung Tahfidz Gadingrejo :</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        @foreach($beranda as $ber)
        @if($ber->beranda_key==='Mengapa')
        {!!$ber->beranda_value!!}
        @endif
        @endforeach
        <br><br>
      </div>
    </div>
    <div class="row" align="center">
      <div class="col-lg-12 col-md-12">
        <div class="site-plan-video">
          @foreach($video as $vid)
          @if($vid->video_key==='Mengapa')
          <iframe class="video-youtube" src="{{$vid->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          @endif
          @endforeach
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12" align="center">
        <a href="#hubungi" class="primary-btn header-btn text-uppercase mt-10">Hubungi Kami</a>
      </div>
    </div>
  </div>
</section>
<!-- Harga FIX -->
<section class="team-area section-gap" id="team">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="menu-content pb-70 col-lg-8">
        <div class="title text-center">
          <h1 class="mb-10">Harga</h1>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12" align="center">
        @foreach($gambar as $image)
        @if($image->gambar_key==='Gambar Harga')
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" width="500px" class="img-thumbnail" alt="Responsive image"><br><br>
        @endif
        @endforeach
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12" align="center"><br>
        <a href="#hubungi" class="primary-btn header-btn text-uppercase mt-10">Hubungi Kami</a>
      </div>
    </div>
  </div>
</section>
<!-- Tentang FIX -->
<section class="feature-area section-gap">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="menu-content pb-60 col-lg-8">
        <div class="title text-center">
          <h1 class="mb-10">Tentang Kami Kampung Tahfidz Ats-Tsaqip</h1>
        </div>
      </div>
    </div>
    <div class="row" align="center">
      @foreach($video as $vid)
      @if($vid->video_key==='Tentang 1')
      <div class="col-lg-3 col-md-6">
        <div class="site-plan-video">
          <iframe width="260" height="180" src="{{$vid->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      @endif
      @if($vid->video_key==='Tentang 2')
      <div class="col-lg-3 col-md-6">
        <div class="site-plan-video">
          <iframe width="260" height="180" src="{{$vid->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      @endif
      @if($vid->video_key==='Tentang 3')
      <div class="col-lg-3 col-md-6">
        <div class="site-plan-video">
          <iframe width="260" height="180" src="{{$vid->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      @endif
      @if($vid->video_key==='Tentang 4')
      <div class="col-lg-3 col-md-6">
        <div class="site-plan-video">
          <iframe width="260" height="180" src="{{$vid->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
      @endif
      @endforeach

    </div>
    <div class="row">
      <div class="col-lg-12" align="center">
        <br><br>
        <a href="#hubungi" class="primary-btn header-btn text-uppercase mt-10">Hubungi Kami</a>
      </div>
    </div>
  </div>
</section>
<hr>
<!-- Hubungi FIX -->
<section class="contact-page-area section-gap" id="hubungi">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="menu-content pb-60 col-lg-8">
        <div class="title text-center">
          <h1 class="mb-10">Hubungi Kami</h1>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 d-flex flex-column address-wrap">
        <div class="single-contact-address d-flex flex-row">
          <div class="icon">
            <span class="lnr lnr-home" style="color: #f58412;"></span>
          </div>
          <div class="contact-details">
            @foreach($profil as $prof)
            @if($prof->profil_key==='Alamat 1')
            <h5>{!! $prof->profil_value !!}</h5>
            @endif
            @if($prof->profil_key==='Alamat 2')
            <p>
              {!! $prof->profil_value !!}
            </p>
            @endif
            @endforeach
          </div>
        </div>
        <div class="single-contact-address d-flex flex-row">
          <div class="icon">
            <span class="lnr lnr-phone-handset" style="color: #f58412;"></span>
          </div>
          <div class="contact-details">
            @foreach($profil as $prof)
            @if($prof->profil_key==='Telp')
            <h5>{!! $prof->profil_value !!}</h5>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Jam Kerja')
            <p>{!! $prof->profil_value !!}</p>
            @endif
            @endforeach
          </div>
        </div>
        <div class="single-contact-address d-flex flex-row">
          <div class="icon">
            <span class="lnr lnr-envelope" style="color: #f58412;"></span>
          </div>
          <div class="contact-details">
            @foreach($profil as $prof)
            @if($prof->profil_key==='Email')
            <h5>{!! $prof->profil_value !!}</h5>
            @endif
            @endforeach
            <p>Kirimi Kami Pertanyaan Anda Kapan Saja!</p>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <form class="form-area " action="/pesan/store" method="post" class="contact-form text-right">
          {{ csrf_field() }}

          <div class="row">
            <div class="col-lg-6 form-group">
              <input name="nama" placeholder="Nama Lengkap" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Lengkap'" class="common-input mb-20 form-control" required="" type="text">

              <input name="email" placeholder="Email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" class="common-input mb-20 form-control" required="" type="email">

              <input name="judul" placeholder="Judul" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Judul'" class="common-input mb-20 form-control" required="" type="text">
            </div>
            <div class="col-lg-6 form-group">
              <textarea class="common-textarea form-control" name="pesan" placeholder="Pesan" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pesan'" required=""></textarea>
            </div>
            <div class="col-lg-12">
              <div class="alert-msg" style="text-align: left;"></div>
              <button name="submit" type="submit" class="genric-btn primary circle" style="float: right;">Kirim Pesan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

@endsection