@extends('layouts.user.user')

@section('content')

<section class="banner-area relative about-banner" id="home">
  <div class="overlay overlay-bg"></div>
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="about-content col-lg-12">
        <h1 class="text-white">
          Lokasi
        </h1>
        <p class="text-white link-nav"><a href="{{url('/')}}">Beranda </a> <span class="lnr lnr-arrow-right"></span> <a href="{{url('/lokasi')}}"> Lokasi</a></p>
      </div>
    </div>
  </div>
</section>

<section class="service-area section-gap">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-md-12 pb-40 header-text text-center">
        <h1 class="pb-10">Lokasi</h1>
        <p>
          Berikut Beberapa Rencana Lokasi Dari Project Yang Kami Miliki
        </p>
      </div>
    </div>
    <div class="row">
      @foreach($gambar as $image)
      @if($image->gambar_key==='Gambar Rencana 1')
      <div class="col-md-4">
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" class="img-fluid" alt="Responsive image"><br><br>
      </div>
      @endif
      @endforeach
      @foreach($gambar as $image)
      @if($image->gambar_key==='Gambar Rencana 2')
      <div class="col-md-4">
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" class="img-fluid" alt="Responsive image"><br><br>
      </div>
      @endif
      @endforeach
      @foreach($gambar as $image)
      @if($image->gambar_key==='Gambar Rencana 3')
      <div class="col-md-4">
        <img src="{{asset('itlabil/images/default/'.$image->gambar_value)}}" class="img-fluid" alt="Responsive image"><br><br>
      </div>
      @endif
      @endforeach
    </div>
    <div class="row">
      <div class="col-lg-12" align="center">
        <a href="/#hubungi" class="primary-btn header-btn text-uppercase mt-10">Hubungi Kami</a>
      </div>
    </div>
  </div>
</section>
@endsection