@extends('layouts.user.user')

@section('content')

<section class="banner-area relative about-banner" id="home">
  <div class="overlay overlay-bg"></div>
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="about-content col-lg-12">
        <h1 class="text-white">
          Hubungi Kami
        </h1>
        <p class="text-white link-nav"><a href="{{url('/')}}">Beranda </a> <span class="lnr lnr-arrow-right"></span> <a href="{{url('/kontak')}}"> Hubungi Kami</a></p>
      </div>
    </div>
  </div>
</section>

<section class="contact-page-area section-gap">
  <div class="container">
    <div class="row">
      <div class="col-lg-12" style="margin-bottom: 50px;">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.272972503111!2d105.0706095147651!3d-5.375283096101108!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e40d283dd921faf%3A0x5968d876c03ffb00!2sJambu%20Kristal%20Gadingrejo!5e0!3m2!1sen!2sid!4v1595856224065!5m2!1sen!2sid" width="100%" height="450px" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
      <div class="col-lg-4 d-flex flex-column address-wrap">
        <div class="single-contact-address d-flex flex-row">
          <div class="icon">
            <span class="lnr lnr-home" style="color: #f58412;"></span>
          </div>
          <div class="contact-details">
            @foreach($profil as $prof)
            @if($prof->profil_key==='Alamat 1')
            <h5>{!! $prof->profil_value !!}</h5>
            @endif
            @if($prof->profil_key==='Alamat 2')
            <p>
              {!! $prof->profil_value !!}
            </p>
            @endif
            @endforeach
          </div>
        </div>
        <div class="single-contact-address d-flex flex-row">
          <div class="icon">
            <span class="lnr lnr-phone-handset" style="color: #f58412;"></span>
          </div>
          <div class="contact-details">
            @foreach($profil as $prof)
            @if($prof->profil_key==='Telp')
            <h5>{!! $prof->profil_value !!}</h5>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Jam Kerja')
            <p>{!! $prof->profil_value !!}</p>
            @endif
            @endforeach
          </div>
        </div>
        <div class="single-contact-address d-flex flex-row">
          <div class="icon">
            <span class="lnr lnr-envelope" style="color: #f58412;"></span>
          </div>
          <div class="contact-details">
            @foreach($profil as $prof)
            @if($prof->profil_key==='Email')
            <h5>{!! $prof->profil_value !!}</h5>
            @endif
            @endforeach
            <p>Kirimi Kami Pertanyaan Anda Kapan Saja!</p>
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <form class="form-area " action="/pesan/store" method="post" class="contact-form text-right">
          {{ csrf_field() }}

          <div class="row">
            <div class="col-lg-6 form-group">
              <input name="nama" placeholder="Nama Lengkap" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Lengkap'" class="common-input mb-20 form-control" required="" type="text">

              <input name="email" placeholder="Email" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email'" class="common-input mb-20 form-control" required="" type="email">

              <input name="judul" placeholder="Judul" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Judul'" class="common-input mb-20 form-control" required="" type="text">
            </div>
            <div class="col-lg-6 form-group">
              <textarea class="common-textarea form-control" name="pesan" placeholder="Pesan" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pesan'" required=""></textarea>
            </div>
            <div class="col-lg-12">
              <div class="alert-msg" style="text-align: left;"></div>
              <button name="submit" type="submit" class="genric-btn primary circle" style="float: right;">Kirim Pesan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>

@endsection