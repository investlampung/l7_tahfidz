<!DOCTYPE html>
<html lang="id">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" href="{{asset('itlabil/images/default/logo-tahfidz.png')}}">
  <title>Admin Tahfidz</title>

  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!-- CSS -->

  <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/morris.js/morris.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="{{ asset('itlabil/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link href="{{ asset('itlabil/admin/toast/toastr.min.css') }}" rel="stylesheet">
  <link href="{{ asset('itlabil/admin/dist/css/editsendiri.css') }}" rel="stylesheet">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- HEADER -->
    <header class="main-header">
      <a href="{{ asset('admin/beranda') }}" class="logo">
        <span class="logo-mini">KTG</span>
        <span class="logo-lg"><b>Tahfidz</b></span>
      </a>
      <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{Auth::user()->name}}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="{{ asset('itlabil/admin/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
                  <p>{{Auth::user()->name}}</p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div align="center">
                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                      {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- MENU SIDE BAR -->
    <aside class="main-sidebar">
      <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
          <li><a href="{{ asset('admin/beranda') }}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
          <li><a href="{{ asset('admin/profil') }}"><i class="fa fa-bank"></i> <span>Profil</span></a></li>
          <li><a href="{{ asset('admin/gambar') }}"><i class="fa fa-image"></i> Gambar</a></li>
          <li><a href="{{ asset('admin/video') }}"><i class="fa fa-play"></i> Video</a></li>
          <li><a href="{{ asset('admin/pesan') }}"><i class="fa fa-comment"></i> Pesan</a></li>
          <li><a href="{{ asset('admin/history') }}"><i class="fa fa-list"></i> <span>History</span></a></li>
        </ul>
      </section>
    </aside>

    <!-- CONTENT -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        @yield('content')
      </section>
    </div>

    <!-- FOOTER -->
    <footer class="main-footer">
      <strong>Copyright &copy; 2020 Kampung Tahfidz Gadingrejo
    </footer>

    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- JS -->
  <script src="{{ asset('itlabil/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <script src="{{ asset('itlabil/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/raphael/raphael.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/morris.js/morris.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/moment/min/moment.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
  <script src="{{ asset('itlabil/admin/dist/js/adminlte.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/dist/js/pages/dashboard.js') }}"></script>
  <script src="{{ asset('itlabil/admin/dist/js/demo.js') }}"></script>


  <!-- DataTables -->
  <script src="{{ asset('itlabil/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('itlabil/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

  <script type="text/javascript" src="{{ asset('itlabil/admin/toast/toastr.min.js') }}"></script>

  <script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"

    switch (type) {
      case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
      case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
      case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
      case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
    }
    @endif
  </script>

  <script>
    $(function() {
      $('#example1').DataTable()
      $('#example2').DataTable()
      $('#example3').DataTable({
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
      })
    })
  </script>

  <script>
    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#category-img-tag').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("#cat_image").change(function() {
      readURL(this);
    });
  </script>
</body>

</html>