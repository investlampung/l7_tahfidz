<div class="icon-side-sosmed">
		@foreach($profil as $prof)
		@if($prof->profil_key==='Whatsapp')
		<a href="{{ $prof->profil_value }}" class="whatsapp"><i class="fa fa-whatsapp"></i></a>
		@endif
		@endforeach
	</div>

	<header id="header" id="home">
		<div class="header-top">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 col-sm-6 col-4 header-top-left no-padding">
						<a href="{{url('/')}}">
							<img src="{{asset('itlabil/images/default/logonn.png')}}" alt="" title="" />
						</a>
					</div>
					<div class="col-lg-6 col-sm-6 col-8 header-top-right no-padding">
						@foreach($profil as $prof)
						@if($prof->profil_key==='Telp')
						<a class="btns" href="tel:{{$prof->profil_value}}">{{$prof->profil_value}}</a>
						@endif
						@endforeach
						@foreach($profil as $prof)
						@if($prof->profil_key==='Email')
						<a class="btns" href="mailto:{{$prof->profil_value}}">{{$prof->profil_value}}</a>
						@endif
						@endforeach
						@foreach($profil as $prof)
						@if($prof->profil_key==='Telp')
						<a class="icons" href="tel:{{$prof->profil_value}}">
							@endif
							@endforeach
							<span class="lnr lnr-phone-handset"></span>
						</a>
						@foreach($profil as $prof)
						@if($prof->profil_key==='Email')
						<a class="icons" href="mailto:{{$prof->profil_value}}">
							@endif
							@endforeach
							<span class="lnr lnr-envelope"></span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div class="container main-menu">
			<div class="row align-items-center justify-content-between d-flex">
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						<li class="menu-active"><a href="{{url('/')}}">Beranda</a></li>
						<li><a href="{{url('/lokasi')}}">Lokasi</a></li>
						<li><a href="{{url('/kontak')}}">Hubungi Kami</a></li>
					</ul>
				</nav><!-- #nav-menu-container -->
				<div class="menu-social-icons">
					@foreach($profil as $prof)
					@if($prof->profil_key==='Facebook')
					<a href="{{$prof->profil_value}}"><i class="fa fa-facebook"></i></a>
					@endif
					@if($prof->profil_key==='Instagram')
					<a href="{{$prof->profil_value}}"><i class="fa fa-instagram"></i></a>
					@endif
					@if($prof->profil_key==='Youtube')
					<a href="{{$prof->profil_value}}"><i class="fa fa-youtube"></i></a>
					@endif
					@if($prof->profil_key==='Whatsapp')
					<a href="{{$prof->profil_value}}"><i class="fa fa-whatsapp"></i></a>
					@endif
					@endforeach
				</div>
			</div>
		</div>
	</header>