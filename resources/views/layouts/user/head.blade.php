  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="{{asset('itlabil/images/default/logo-tahfidz.png')}}">
	<meta name="author" content="colorlib">
	<meta name="description" content="Kampung Tahfidz Gadingrejo">
	<meta name="keywords" content="">
	<meta charset="UTF-8">
	<title>Kampung Tahfidz Gadingrejo</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/custom.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/linearicons.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/bootstrap.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/magnific-popup.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/nice-select.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/animate.min.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/jquery-ui.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('itlabil/user/css/main.css')}}">
  <link href="{{ asset('itlabil/admin/toast/toastr.min.css') }}" rel="stylesheet">
	<style>
		.banner-area {
			@foreach($gambar as $image) 
			@if($image->gambar_key==='Gambar Slide') 
			background: url({{asset('itlabil/images/default/'.$image->gambar_value)}}) center;
			@endif 
			@endforeach background-size: cover;
			background-attachment: fixed
		}

		.home-about-area:after {
			position: absolute;
			content: "";
			top: 0;
			left: 0;
			width: 48%;
			height: 100%;
			@foreach($gambar as $image) 
			@if($image->gambar_key==='Gambar Fasilitas') 
			background: url({{asset('itlabil/images/default/'.$image->gambar_value)}}) no-repeat center center/cover;
			@endif 
			@endforeach 
			z-index: 1
    }
    .about-banner{
      @foreach($gambar as $image) 
			@if($image->gambar_key==='Gambar Header') 
      background:url({{asset('itlabil/images/default/'.$image->gambar_value)}}) center
      @endif 
			@endforeach 
    }
	</style>