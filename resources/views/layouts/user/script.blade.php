<script src="{{asset('itlabil/user/js/vendor/jquery-2.2.4.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="{{asset('itlabil/user/js/vendor/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="{{asset('itlabil/user/js/easing.min.js')}}"></script>
<script src="{{asset('itlabil/user/js/hoverIntent.js')}}"></script>
<script src="{{asset('itlabil/user/js/superfish.min.js')}}"></script>
<script src="{{asset('itlabil/user/js/jquery.ajaxchimp.min.js')}}"></script>
<script src="{{asset('itlabil/user/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('itlabil/user/js/jquery-ui.js')}}"></script>
<script src="{{asset('itlabil/user/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('itlabil/user/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('itlabil/user/js/mail-script.js')}}"></script>
<script src="{{asset('itlabil/user/js/main.js')}}"></script>
<script type="text/javascript" src="{{ asset('itlabil/admin/toast/toastr.min.js') }}"></script>

<script>
  @if(Session::has('message'))
  var type = "{{Session::get('alert-type','info')}}"

  switch (type) {
    case 'info':
      toastr.info("{{ Session::get('message') }}");
      break;
    case 'success':
      toastr.success("{{ Session::get('message') }}");
      break;
    case 'warning':
      toastr.warning("{{ Session::get('message') }}");
      break;
    case 'error':
      toastr.error("{{ Session::get('message') }}");
      break;
  }
  @endif
</script>