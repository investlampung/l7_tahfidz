<footer class="footer-area section-gap">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6">
        <div class="single-footer-widget">
          <h6>Kampung Tahfidz Gadingrejo</h6>
          @foreach($beranda as $ber)
          @if($ber->beranda_key==='Kampung Tahfidz')
          {!!$ber->beranda_value!!}
          @endif
          @endforeach
        </div>
      </div>
      <div class="col-lg-6 col-md-6">
        <div class="single-footer-widget mail-chimp">
          <h6 class="mb-20">Hubungi Kami</h6>
          @foreach($profil as $prof)
          @if($prof->profil_key==='Alamat 1')
          {!! $prof->profil_value !!}
          @endif
          @if($prof->profil_key==='Alamat 2')
          {!! $prof->profil_value !!}
          @endif
          @endforeach
          <br><br>
          @foreach($profil as $prof)
          @if($prof->profil_key==='Telp')
          <h3>{{$prof->profil_value}}</h3>
          @endif
          @if($prof->profil_key==='Email')
          <h3>{{$prof->profil_value}}</h3>
          @endif
          @endforeach
        </div>
      </div>
    </div>

    <div class="row footer-bottom d-flex justify-content-between">
      <p class="col-lg-8 col-sm-12 footer-text m-0">
        Copyright &copy; 2020 Kampung Tahfidz Gadingrejo | Source Media
      </p>
      <div class="col-lg-4 col-sm-12 footer-social">
        @foreach($profil as $prof)
        @if($prof->profil_key==='Facebook')
        <a href="{{$prof->profil_value}}"><i class="fa fa-facebook"></i></a>
        @endif
        @if($prof->profil_key==='Instagram')
        <a href="{{$prof->profil_value}}"><i class="fa fa-instagram"></i></a>
        @endif
        @if($prof->profil_key==='Youtube')
        <a href="{{$prof->profil_value}}"><i class="fa fa-youtube"></i></a>
        @endif
        @if($prof->profil_key==='Whatsapp')
        <a href="{{$prof->profil_value}}"><i class="fa fa-whatsapp"></i></a>
        @endif
        @endforeach
      </div>
    </div>
  </div>
</footer>