<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	@include('layouts.user.head')
</head>

<body>
	@include('layouts.user.header')

	@yield('content')

	@include('layouts.user.footer')

	@include('layouts.user.script')

</body>

</html>