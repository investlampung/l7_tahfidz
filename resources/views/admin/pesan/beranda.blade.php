@extends('layouts.admin')

@section('content')

<section class="content-header">
  <h1>
    Pesan
  </h1>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pesan</h3>
        </div>

        <div class="box-body">
          <div class="box-body" style="overflow-x:auto;">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Judul</th>
                  <th>Pesan</th>
                  <th>Tanggal</th>
                </tr>
              </thead>
              <tbody>
                @foreach( $data as $item )
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ $item->judul }}</td>
                  <td>{{ $item->pesan }}</td>
                  <td>{{ $item->created_at->format('d-m-Y, h:m:s') }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection