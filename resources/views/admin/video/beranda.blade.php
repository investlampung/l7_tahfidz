@extends('layouts.admin')

@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Video
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Video</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $item->video_key }}</td>
                                <td> <iframe width="260" height="180" src="{{$item->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>
                                <td align="center">
                                    <a href="{{ route('video.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection