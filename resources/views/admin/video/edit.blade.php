@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Video
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Video</h3>
        </div>
        <form action="{{ route('video.update',$video->id) }}" class="form-horizontal" method="POST">
          @csrf
          @method('PUT')
          <div class="box-body">
            <div class="form-group">
              <label for="{{$video->video_key}}" class="col-sm-2 control-label">{{$video->video_key}}</label>

              <div class="col-sm-10">
                <input type="text" name="video_value" value="{{$video->video_value}}" class="form-control" placeholder="{{$video->video_key}}">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/video')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection