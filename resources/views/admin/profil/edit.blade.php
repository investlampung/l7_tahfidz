@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Profil
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Profil</h3>
        </div>
        <form action="{{ route('profil.update',$profil->id) }}" class="form-horizontal" method="POST">
          @csrf
          @method('PUT')
          <div class="box-body">
            <div class="form-group">
              <label for="{{$profil->profil_key}}" class="col-sm-2 control-label">{{$profil->profil_key}}</label>

              <div class="col-sm-10">
                <input type="text" name="profil_value" value="{{$profil->profil_value}}" class="form-control" placeholder="{{$profil->profil_key}}">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/profil')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection