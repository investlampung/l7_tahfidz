<?php

namespace App\Http\Controllers\Web\User;

use App\Beranda;
use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Pesan;
use App\Profil;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BerandaController extends Controller
{
    public function index()
    {
        $beranda = Beranda::all();
        $gambar = Gambar::all();
        $profil = Profil::all();
        $video = Video::all();

        return view('user.beranda',compact('beranda','gambar','profil','video'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = $request->only('nama', 'pesan', 'judul', 'email');

        Pesan::create($data);

        // HISTORI
        $histori['user'] = $request->nama;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "User " . $request->nama . " mengirim pesan " . $request->pesan;
        History::create($histori);

        $notification = array(
            'message' => 'Pesan berhasil dikirim',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
