<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Str;

class GambarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = Gambar::all();
        return view('admin.gambar.beranda', compact('data'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Gambar $gambar)
    {
        return view('admin.gambar.edit', compact('gambar'));
    }

    public function update(Request $request, $id)
    {
        $gambar = Gambar::findOrFail($id);

        $datagambar = $request->only('gambar_key');

        if ($request->hasFile('gambar_value')) {
            $this->deletePhoto($gambar->gambar_value);
            $datagambar['gambar_value'] = $this->savePhoto($request->file('gambar_value'));
        }

        $gambar->update($datagambar);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Gambar " . $gambar->gambar_key . " menjadi " . $gambar->gambar_value;
        History::create($histori);

        $notification = array(
            'message' => 'Gambar "' . $gambar->gambar_key . '" berhasil diubah menjadi ' . $gambar->gambar_value,
            'alert-type' => 'success'
        );

        return redirect()->route('gambar.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/images/default';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/default/' . $filename;
        return File::delete($path);
    }
}
