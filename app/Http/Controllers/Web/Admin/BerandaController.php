<?php

namespace App\Http\Controllers\Web\Admin;

use App\Beranda;
use App\History;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $beranda = Beranda::all();
        return view('admin.beranda.beranda', compact('beranda'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit(Beranda $beranda)
    {
        return view('admin.beranda.edit', compact('beranda'));
    }

    public function update(Request $request, Beranda $beranda)
    {
        $beranda->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Beranda " . $beranda->beranda_key . " menjadi " . $beranda->beranda_value;
        History::create($histori);

        $notification = array(
            'message' => 'Beranda "' . $beranda->beranda_key . '" berhasil diubah menjadi ' . $beranda->beranda_value,
            'alert-type' => 'success'
        );

        return redirect()->route('beranda.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
