<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesan extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at'];
}
