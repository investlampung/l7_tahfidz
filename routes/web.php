<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Web\User\BerandaController@index');
Route::resource('/lokasi', 'Web\User\LokasiController');
Route::resource('/kontak', 'Web\User\KontakController');
Route::post('/pesan/store', 'Web\User\BerandaController@store');

Auth::routes();

Route::resource('/admin/beranda', 'Web\Admin\BerandaController');
Route::resource('/admin/profil', 'Web\Admin\ProfilController');
Route::resource('/admin/gambar', 'Web\Admin\GambarController');
Route::resource('/admin/video', 'Web\Admin\VideoController');
Route::resource('/admin/pesan', 'Web\Admin\PesanController');
Route::resource('/admin/history', 'Web\Admin\HistoryController');
