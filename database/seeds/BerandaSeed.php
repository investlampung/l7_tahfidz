<?php

use Illuminate\Database\Seeder;

class BerandaSeed extends Seeder
{

    public function run()
    {
        App\Beranda::create([
            'beranda_key' => 'Text Slide',
            'beranda_value' => 'Kavling Rumah/Villa<br>LUAS 5 - 10 Ha'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Kampung Tahfidz',
            'beranda_value' => 'Adalah rintisan sebuah kampung modern yang berbasis komunitas bagi keluarga pecinta dan penghafal al quran.<br>Rumah Tahfidz Ats-Tsaqip sebagai ikon & energi positif akan mewarnai aktifitas kampung tahfidz.<br>Dikampung ini akan didirikan SDIT Ats-Tsaqip, rumah ustad dan masjid.<br>Terletak di pekon gadingrejo masuk gading tahu di area sekitar 5 s.d. 10 hektar.'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Rencana',
            'beranda_value' => 'Bagi sebagian orang berkumpul dengan orang2 soleh khususnya keluarga pecinta & penghafal al quran, adalah tatanan kehidupan yang lebih syari di tengah hiruk pikuk zaman yg cenderung sekuler, kita akan bersaudara, bertetangga dengan saudara kita yg mempunyai cara pandang sama, anak anak kita akan bergaul di lingkungan yang terdidik secara syari.<br>Untuk mewujudkan kampung tahfidz gadingrejo, manajemen pengelola telah membuat skedul, jangka panjang sampai terbentuknya kampung tahfidz yang nyaman, asri dan modern.'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Fasilitas',
            'beranda_value' => 'Direncana diatas lahan 5 ha sd. 10 ha dan bisa dihuni sekitar 200kk - 400kk, kami memberi kesempatan kepada halayak umum untuk membentuk komunitas hunian "berkumpul dengan orang-orang Sholeh"<br><br>Fasilitas:<br>Dibangun infrastuktur jalan, drainase<br>Dibangun SDIT Ats-Tsaqip<br>Dibangun masjid<br>Dibangun rumah ustad'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Mengapa',
            'beranda_value' => 'Keluarga Ats-Tsaqip<br>Konsep : membuat perumahan modern untuk keluarga rumah tahfidz<br>Kelebihan :<br><li>Sesuai kemampuan mulai dari 500rb per bulan.</li><br><li>Dp semampunya.</li><br><li>Dibangun infrastruktur jalan, drainase, jaringan internet, jaringan listrik.</li><br><li>Penduduk 200 kk.</li><br><br>Akan Diwakafkan Ke Yayasan<br>1. Bidang tanah 1/2- 1 ha tanah<br>2. Dibangun masjid<br>3. Dibangun lokal sdit 6 lokal<br>4. dibangun rumah ustadz 3 unit'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Harga',
            'beranda_value' => 'Harga dicicil: 40jt<br>Harga tunai : 35jt<br>Harga cash tempo : 37jt (dicicil 6 bln)<br>Surat skt dari pekon.'
        ]);
    }
}
