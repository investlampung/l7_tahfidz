<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{

    public function run()
    {
        App\User::create([
            'email' => 'admin@sourcemedia.id',
            'name' => 'Administrator',
            'password' => bcrypt('1sampai9')
        ]);
        App\User::create([
            'email' => 'admin@kampungtahfidzgadingrejo.com',
            'name' => 'Admin Tahfidz',
            'password' => bcrypt('1sampai2')
        ]);
    }
}
