<?php

use Illuminate\Database\Seeder;

class GambarSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Gambar::create([
            'gambar_key' => 'Gambar Slide',
            'gambar_value' => 'bg-header.jpg'
        ]);
        App\Gambar::create([
            'gambar_key' => 'Gambar Header',
            'gambar_value' => 'bg-header-top.jpg'
        ]);
        App\Gambar::create([
            'gambar_key' => 'Gambar Fasilitas',
            'gambar_value' => 'fasilitas.jpg'
        ]);
        App\Gambar::create([
            'gambar_key' => 'Gambar Rencana 1',
            'gambar_value' => 'gading.JPG'
        ]);
        App\Gambar::create([
            'gambar_key' => 'Gambar Rencana 2',
            'gambar_value' => 'gading.JPG'
        ]);
        App\Gambar::create([
            'gambar_key' => 'Gambar Rencana 3',
            'gambar_value' => 'gading.JPG'
        ]);
    }
}
