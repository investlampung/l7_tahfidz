<?php

use Illuminate\Database\Seeder;

class VideoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Video::create([
            'video_key' => 'Mengapa',
            'video_value' => 'https://www.youtube.com/embed/or78hxzV1Xc'
        ]);
        App\Video::create([
            'video_key' => 'Tentang 1',
            'video_value' => 'https://www.youtube.com/embed/7iJxex5gbu0'
        ]);
        App\Video::create([
            'video_key' => 'Tentang 2',
            'video_value' => 'https://www.youtube.com/embed/yQ9-E15Du2o'
        ]);
        App\Video::create([
            'video_key' => 'Tentang 3',
            'video_value' => 'https://www.youtube.com/embed/CtNm43k2dbQ'
        ]);
        App\Video::create([
            'video_key' => 'Tentang 4',
            'video_value' => 'https://www.youtube.com/embed/yQ9-E15Du2o'
        ]);
    }
}
