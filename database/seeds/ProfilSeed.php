<?php

use Illuminate\Database\Seeder;

class ProfilSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Profil::create([
            'profil_key' => 'Nama',
            'profil_value' => 'Kampung Tahfidz Gadingrejo'
        ]);
        App\Profil::create([
            'profil_key' => 'Alamat 1',
            'profil_value' => 'Gadingrejo, Pringsewu'
        ]);
        App\Profil::create([
            'profil_key' => 'Alamat 2',
            'profil_value' => 'Jl. Satria Gading Rejo Kec. Gading Rejo Kabupaten Pringsewu Lampung 35372'
        ]);
        App\Profil::create([
            'profil_key' => 'Jam Kerja',
            'profil_value' => 'Senin 08:00 - 17:00 WIB'
        ]);
        App\Profil::create([
            'profil_key' => 'Email',
            'profil_value' => 'mitragriya01@gmail.com'
        ]);
        App\Profil::create([
            'profil_key' => 'Telp',
            'profil_value' => '082269400528'
        ]);
        App\Profil::create([
            'profil_key' => 'Youtube',
            'profil_value' => '#'
        ]);
        App\Profil::create([
            'profil_key' => 'Instagram',
            'profil_value' => '#'
        ]);
        App\Profil::create([
            'profil_key' => 'Facebook',
            'profil_value' => '#'
        ]);
        App\Profil::create([
            'profil_key' => 'Whatsapp',
            'profil_value' => '#'
        ]);
    }
}
