<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeed::class);
        $this->call(BerandaSeed::class);
        $this->call(GambarSeed::class);
        $this->call(ProfilSeed::class);
        $this->call(VideoSeed::class);
    }
}
